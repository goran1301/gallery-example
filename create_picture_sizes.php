<?php
/*
Console command: php create_picture_sizes.php
creates PictureSize DB items from config (config/pictures.php)
*/

use Doctrine\ORM\EntityManager;
use Entity\PictureSize;

require_once "bootstrap.php";
require_once "config/pictures.php";

/** @var EntityManager $entityManager */
/** @var array[] $sizes */

foreach ($sizes as $size) {

    $sizeItem = new PictureSize($size[1], $size[2], $size[0]);
    try {
        $entityManager->persist($sizeItem);
        $entityManager->flush();

        echo ok($sizeItem)."\n";
    } catch (Exception $e) {
        echo $e->getMessage()."\n";
    }
}

/**
 * Generates message of PictureSize creation
 *
 * @param PictureSize $size created picture size item
 * @return string item created message
 */
function ok(PictureSize $size): string {
    return $size->getCode(). ' ('. $size->getWidth(). ' x '. $size->getHeight().") done.";
}