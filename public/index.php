<?php

use Controller\SiteController;
use Doctrine\ORM\EntityManager;
/** @var EntityManager $entityManager */
require_once "../bootstrap.php";

echo (new SiteController($entityManager))->index();