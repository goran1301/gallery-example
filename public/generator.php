<?php

use Controller\ImageController;
use Doctrine\ORM\EntityManager;
/** @var EntityManager $entityManager */
require_once "../bootstrap.php";

echo (new ImageController($entityManager))->generatePreview();