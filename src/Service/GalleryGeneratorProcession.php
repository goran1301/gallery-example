<?php

namespace Service;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entity\PictureSize;
use Exception\CacheImageException;
use Exception\FormatNotSupportedException;
use Exception\NoImageFoundException;
use Exception\ResizeFailedException;
use Repository\PictureSizeRepository;

/**
 * Class GalleryGeneratorProcession
 * @package Service
 */
class GalleryGeneratorProcession
{
    /**
     * @var PictureSizeRepository
     */
    private PictureSizeRepository $pictureSizeRepository;

    /**
     * @var string
     */
    private string $sourceFolder;

    /**
     * @var string
     */
    private string $cacheFolder;

    /**
     * @var ImageCacheManager
     */
    private ImageCacheManager $imageCacheManager;

    /**
     * @var ImageResizer
     */
    private ImageResizer $imageResizer;

    /**
     * @var ImageLoader
     */
    private ImageLoader $imageLoader;

    /**
     * GalleryGeneratorProcession constructor.
     *
     * @param PictureSizeRepository $pictureSizeRepository
     * @param ImageCacheManager $imageCacheManager
     * @param ImageResizer $imageResizer
     * @param ImageLoader $imageLoader
     * @param string $sourceFolder
     * @param string $cacheFolder
     */
    public function __construct(
        PictureSizeRepository $pictureSizeRepository,
        ImageCacheManager $imageCacheManager,
        ImageResizer $imageResizer,
        ImageLoader $imageLoader,
        string $sourceFolder,
        string $cacheFolder
    )
    {
        $this->pictureSizeRepository = $pictureSizeRepository;
        $this->imageCacheManager = $imageCacheManager;
        $this->imageResizer = $imageResizer;
        $this->imageLoader = $imageLoader;
        $this->sourceFolder = $sourceFolder;
        $this->cacheFolder = $cacheFolder;
    }

    /**
     * @param string $baseName
     * @param string $sizeCode
     * @return string
     * @throws CacheImageException
     * @throws FormatNotSupportedException
     * @throws NoImageFoundException
     * @throws ResizeFailedException
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getImage(string $baseName, string $sizeCode)
    {
        $size = $this->pictureSizeRepository->getByCode($sizeCode);
        $cacheName = $this->getCacheName($baseName, $size);
        try {
            return $this->imageCacheManager->getImageFromCache($cacheName);
        } catch (CacheImageException $e) {
            $sourceImage = $this->imageLoader->loadSourceImage($this->sourceFolder.'/'.$baseName);
            $resizedImage = $this->imageResizer->resizeByMaxSize($sourceImage, $size->getWidth(), $size->getHeight());
            $this->imageCacheManager->cacheImage($resizedImage, $cacheName);

            return $this->imageCacheManager->getImageFromCache($cacheName);
        }
    }

    /**
     * @param string $baseName
     * @param PictureSize $size
     * @return string
     */
    private function getCacheName(string $baseName, PictureSize $size): string
    {
        return $this->cacheFolder.'/'.$baseName.$size->getSizeByString().'.jpg';
    }
}