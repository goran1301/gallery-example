<?php

namespace Service;

/**
 * Class Image
 * @package Service
 */
class Image
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $format;

    /**
     * @var string
     */
    private string $path;

    /**
     * @var int
     */
    private int $width;

    /**
     * @var int
     */
    private int $height;

    /**
     * Image constructor.
     *
     * @param string $name
     * @param string $format
     * @param string $path
     * @param int $width
     * @param int $height
     */
    public function __construct(string $name, string $format, string $path, int $width, int $height)
    {
        $this->format = $format;
        $this->name = $name;
        $this->path = $path;
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param float $scale
     * @return float
     */
    public function getScaledWidth(float $scale): float
    {
        return $this->width * $scale;
    }

    /**
     * @param float $scale
     * @return float
     */
    public function getScaledHeight(float $scale): float
    {
        return $this->height * $scale;
    }
}