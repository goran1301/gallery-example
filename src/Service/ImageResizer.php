<?php

namespace Service;

use Exception\FormatNotSupportedException;
use Exception\ResizeFailedException;

/**
 * Class ImageResizer
 * @package Service
 */
class ImageResizer
{

    /**
     * Resize image
     *
     * @param Image $image
     * @param int $maxWidth
     * @param int $maxHeight
     *
     * @return resource
     * @throws FormatNotSupportedException
     * @throws ResizeFailedException
     */
    public function resizeByMaxSize(Image $image, int $maxWidth, int $maxHeight)
    {
        return $this->resizeByScale(
            $image,
            $this->getProportionalResizeScale($image->getWidth(), $image->getHeight(), $maxWidth, $maxHeight)
        );
    }

    /**
     * @param Image $image
     * @param float $scale
     *
     * @return resource
     *
     * @throws FormatNotSupportedException
     * @throws ResizeFailedException
     */
    public function resizeByScale(Image $image, float $scale)
    {
        $source = $this->imageCreateFromImage($image);
        $thumb = imagecreatetruecolor($image->getScaledWidth($scale), $image->getScaledHeight($scale));
        if (!imagecopyresized(
            $thumb,
            $source,
            0,
            0,
            0,
            0,
            $image->getScaledWidth($scale),
            $image->getScaledHeight($scale),
            $image->getWidth(),
            $image->getHeight()
        )) {
            throw new ResizeFailedException();
        }

        return $thumb;
    }

    /**
     * @param Image $image
     * @return resource
     * @throws FormatNotSupportedException
     */
    public function imageCreateFromImage(Image $image)
    {
        switch ($image->getFormat()) {
            case 'jpg':
                return imagecreatefromjpeg($image->getPath());
            case 'png':
                return imagecreatefrompng($image->getPath());
            case 'gif':
                return imagecreatefromgif($image->getPath());
            case 'bmp':
                return imagecreatefrombmp($image->getPath());
        }
        throw new FormatNotSupportedException();
    }

    /**
     * @param int $sourceWidth
     * @param int $sourceHeight
     * @param int $maxWidth
     * @param int $maxHeight
     * @return float
     */
    public function getProportionalResizeScale(int $sourceWidth, int $sourceHeight, int $maxWidth, int $maxHeight): float
    {
        $widthScale = $maxWidth / $sourceWidth;
        $heightScale = $maxHeight / $sourceHeight;
        return $widthScale < $heightScale ? $widthScale : $heightScale;
    }
}