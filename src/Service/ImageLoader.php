<?php

namespace Service;

use Exception\NoImageFoundException;

/**
 * Class ImageLoader
 * @package Service
 */
class ImageLoader
{
    /**
     * @param string $path
     * @return Image
     * @throws NoImageFoundException
     */
    public function loadSourceImage(string $path): Image
    {
        $pathArray = glob($path.'.*');

        if (!is_array($pathArray) || !isset($pathArray[0])) {
            throw new NoImageFoundException();
        }
        $pathInfo = pathinfo($pathArray[0]);
        list($width, $height) = getimagesize($pathArray[0]);
        //var_dump($pathInfo); die();
        return new Image($pathInfo['filename'], $pathInfo['extension'], $pathInfo['dirname'].'/'.$pathInfo['basename'], $width, $height);
    }
}