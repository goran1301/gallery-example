<?php

namespace Service;

use Exception\CacheImageException;

/**
 * Class ImageCacheManager
 * @package Service
 */
class ImageCacheManager
{
    /**
     * @param resource $imageResource
     * @param string $path
     * @throws CacheImageException
     */
    public function cacheImage($imageResource, string $path): void
    {
        if (!imagejpeg($imageResource, $path)) {
            throw new CacheImageException('caching error');
        };
    }

    /**
     * @param string $path
     * @return string
     *
     * @throws CacheImageException
     */
    public function getImageFromCache(string $path): string
    {
        if (!file_exists($path)) {
            throw new CacheImageException('getting from cache error');
        }
        $content =  file_get_contents($path);
        if ($content === false) {
            throw new CacheImageException('getting from cache error');
        }

        return $content;
    }
}