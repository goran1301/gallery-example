<?php

namespace Service;

use Entity\PictureSize;
use Repository\PictureSizeRepository;

/**
 * Class Gallery
 * @package Service
 */
class Gallery
{
    /**
     * @var Device
     */
    private Device $device;

    /**
     * @var PictureSizeRepository
     */
    private PictureSizeRepository $pictureSizeRepository;

    /**
     * @var PictureSize[]
     */
    private ?array $pictureSizes;

    /**
     * @var string[]
     */
    private array $imageList = [];

    private const DESKTOP_PREVIEW = 'min';
    private const MOBILE_PREVIEW = 'mic';

    /**
     * Gallery constructor.
     * @param Device $device
     * @param PictureSizeRepository $pictureSizeRepository
     */
    public function __construct(Device $device, PictureSizeRepository $pictureSizeRepository)
    {
        $this->device = $device;
        $this->pictureSizeRepository = $pictureSizeRepository;
    }

    /**
     * @return string
     */
    public function previewSizeCode(): string
    {
        return $this->device->isDesktop() ? self::DESKTOP_PREVIEW : self::MOBILE_PREVIEW;
    }

    /**
     * @return PictureSize[]
     */
    public function detailedSizes(): array
    {
        if ($this->pictureSizes === null) {
            $this->pictureSizes = $this->pictureSizeRepository->getSizesExcept([
                self::DESKTOP_PREVIEW, self::MOBILE_PREVIEW
            ]);
        }

        return $this->pictureSizes;
    }

    /**
     * @return string[]
     */
    public function detailedSizesCodes(): array
    {
        return array_map(function (PictureSize $pictureSize){
            return $pictureSize->getCode();
        }, $this->detailedSizes());
    }

    /**
     * @return string[]
     */
    public function getImageList(): array
    {
        return $this->imageList;
    }

    /**
     * @param string[] $imageList
     */
    public function setImageList(array $imageList): void
    {
        $this->imageList = $imageList;
    }

}