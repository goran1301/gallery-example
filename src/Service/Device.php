<?php

namespace Service;

/**
 * Class Device
 * @package Service
 */
class Device
{
    /**
     * @var string
     */
    private string $info;

    /**
     * @var bool
     */
    private bool $isMobile;

    /**
     * Device constructor
     */
    public function __construct()
    {
        $this->info = (string) $_SERVER["HTTP_USER_AGENT"];
        $this->isMobile = (bool)preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    /**
     * @return bool
     */
    public function isMobile(): bool
    {
        return $this->isMobile;
    }

    /**
     * @return bool
     */
    public function isDesktop(): bool
    {
        return !$this->isMobile;
    }

}