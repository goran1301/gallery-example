<?php

namespace Controller;

use Core\Exception\ViewException;
use Core\Response;

/**
 * Class SiteController
 * @package Controller
 */
class SiteController extends Controller
{
    /**
     * @return Response
     * @throws ViewException
     */
    public function index(): Response
    {
        return $this->render('site/index');
    }
}