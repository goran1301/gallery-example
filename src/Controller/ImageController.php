<?php

namespace Controller;

use Core\Exception\ApplicationException;
use Core\Exception\ViewException;
use Core\Response;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entity\PictureSize;
use Exception\CacheImageException;
use Exception\FormatNotSupportedException;
use Exception\NoImageFoundException;
use Exception\ResizeFailedException;
use Factory\GalleryGeneratorProcessionFactory;
use Repository\PictureSizeRepository;
use Service\Device;
use Service\Gallery;

/**
 * Class ImageController
 * @package Controller
 */
class ImageController extends Controller
{
    /**
     * @var PictureSizeRepository
     */
    private PictureSizeRepository $pictureSizeRepository;

    /**
     * ImageController constructor.
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        /** @var PictureSizeRepository $pictureSizeRepository */
        $pictureSizeRepository = $this->getEntityManager()->getRepository(PictureSize::class);
        $this->pictureSizeRepository = $pictureSizeRepository;
    }

    /**
     * @return Response
     * @throws ApplicationException
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws CacheImageException
     * @throws FormatNotSupportedException
     * @throws NoImageFoundException
     * @throws ResizeFailedException
     */
    public function generatePreview(): Response
    {
        header('Content-type: image/jpeg');
        $imageName = $this->getImageName();
        $sizeCode = $this->getSizeCode();
        if (empty($imageName) || empty($sizeCode)) {
            throw new ApplicationException('wrong request');
        }

        $generatorFactory = new GalleryGeneratorProcessionFactory();
        $generator = $generatorFactory->make($this->pictureSizeRepository, 'gallery', 'cache');

        return new Response($generator->getImage($imageName, $sizeCode));
    }

    /**
     * @return Response
     * @throws ViewException
     */
    public function gallery(): Response
    {
        $gallery = new Gallery(
            new Device(),
            $this->pictureSizeRepository
        );
        $gallery->setImageList($this->testImageList());

        return $this->render('gallery/gallery', [
            'gallery' => $gallery,
        ]);
    }

    /**
     * @return string[]
     */
    private function testImageList(): array
    {
        $list = [];
        for ($i = 0; $i < 10; $i++) {
            $list[] = (string)$i;
        }

        return $list;
    }

    /**
     * image name form get parameter
     *
     * @return string|null
     */
    private function getImageName(): ?string
    {
        return isset($_GET['image']) ? $_GET['image'] : null;
    }

    /**
     * @return string|null
     */
    private function getSizeCode(): ?string
    {
        return isset($_GET['size']) ? $_GET['size'] : null;
    }
}