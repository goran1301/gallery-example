<?php

namespace Controller;

use Core\Controller as CoreController;
use Core\Response;
use Doctrine\ORM\EntityManager;

class Controller extends CoreController
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager, "../src/view/");
    }

    protected function render(string $viewPath, array $parameters = [], ?string $template = 'template/main'): Response
    {
        return parent::render($viewPath, $parameters, $template);
    }
}