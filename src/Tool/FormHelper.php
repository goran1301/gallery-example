<?php

namespace Tool;

use Core\Exception\WrongCsrfTokenException;
use Core\FormHelper as CoreFormHelper;
use Core\Session;

class FormHelper extends CoreFormHelper
{
    /**
     * FormHelper constructor.
     * @throws WrongCsrfTokenException
     */
    public function __construct()
    {
        $session = new Session();
        $csrf = $session->get('csrf');
        if ($csrf === null) {
            throw new WrongCsrfTokenException('no csrf token exists');
        }
        parent::__construct($session->get('csrf'));
    }
}