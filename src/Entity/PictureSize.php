<?php

namespace Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * Class PictureSize
 * @package Entity
 *
 * @ORM\Entity(repositoryClass="Repository\PictureSizeRepository")
 * @ORM\Table(
 *     name="picture_sizes",
 *     indexes = {
 *         @Index(name="size_index", columns={"width", "height"})
 *     }
 * )
 */
class PictureSize
{
    /**
     * @var int id в таблице
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @var int ширина изображения в пикселях
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $width;

    /**
     * @var int высота изображения в пикселях
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $height;

    /**
     * @var string название формата
     *
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    private string $code;

    /**
     * PictureSize constructor.
     *
     * @param int $width ширина изображения в пикселях
     * @param int $height высота изображения в пикселях
     * @param string $code название формата
     */
    public function __construct(int $width, int $height, string $code)
    {
        $this->width = $width;
        $this->height = $height;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSizeByString(): string
    {
        return $this->width.'x'.$this->height;
    }
}