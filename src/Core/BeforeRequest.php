<?php

namespace Core;

use Core\Exception\WrongCsrfTokenException;

/**
 * Class BeforeRequest
 * @package Core
 */
class BeforeRequest
{
    /**
     * When every request begins
     *
     * @throws WrongCsrfTokenException
     */
    public static function job()
    {
        session_start();
        $session = new Session();
        $session->setIfEmpty('csrf', uniqid('csrf_'));

        if (!empty($_POST) && $_POST['csrf'] !== $session->get('csrf')) {
            throw new WrongCsrfTokenException();
        }
    }
}