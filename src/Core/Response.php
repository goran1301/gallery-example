<?php

namespace Core;

/**
 * Class Response
 * @package Core
 */
class Response
{
    /**
     * @var string
     */
    private string $body;

    /**
     * Response constructor.
     *
     * @param string $body
     */
    public function __construct(string $body)
    {
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->body;
    }
}