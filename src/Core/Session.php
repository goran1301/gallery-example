<?php

namespace Core;

/**
 * Class Session
 * @package Core
 */
class Session
{
    /**
     * Returns data from session by key, or default value
     *
     * @param string $key
     * @param mixed|null $default
     * @return mixed|null
     */
    public function get(string $key, $default = null)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
    }

    /**
     * Set value for a key to a session
     *
     * @param string $key
     * @param mixed $value
     */
    public function set(string $key, $value): void
    {
        $_SESSION[$key] = $value;
    }

    /**
     * Set value for a key to a session if it was empty before
     *
     * @param string $key
     * @param $value
     */
    public function setIfEmpty(string $key, $value): void
    {
        if (empty($_SESSION[$key])) {
            $_SESSION[$key] = $value;
        }
    }

}