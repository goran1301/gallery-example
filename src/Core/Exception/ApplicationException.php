<?php

namespace Core\Exception;

use Exception;

/**
 * Application-level exception
 *
 * Class ApplicationException
 * @package Core\Exception
 */
class ApplicationException extends Exception
{

}