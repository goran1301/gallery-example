<?php

namespace Core\Exception;

/**
 * View-level exception
 *
 * Class ViewException
 * @package Core\Exception
 */
class ViewException extends ApplicationException
{

}