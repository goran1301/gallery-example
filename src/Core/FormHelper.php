<?php

namespace Core;

/**
 * Class FormHelper
 * @package Core
 */
class FormHelper
{
    /**
     * @var string
     */
    private string $csrf;

    /**
     * FormHelper constructor.
     *
     * @param string $csrf
     */
    public function __construct(string $csrf)
    {
        $this->csrf = $csrf;
    }

    /**
     * Render csrf token field
     *
     * @return string
     */
    public function makeCsrfTokenField(): string
    {
        return "<input type='hidden' name='csrf' value='".$this->csrf."'>";
    }
}