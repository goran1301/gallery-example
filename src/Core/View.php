<?php

namespace Core;

use Core\Exception\ViewException;

/**
 * Class View
 * @package Core
 */
class View
{
    /**
     * @var array parameter passed to view
     */
    private array $parameters;

    /**
     * @var string path to view
     */
    private string $viewPath;

    /**
     * @var string|null path to template
     */
    private ?string $templatePath;

    /**
     * View constructor.
     *
     * @param string $viewPath
     * @param string $templatePath
     * @param array $parameters
     */
    public function __construct(string $viewPath, ?string $templatePath, array $parameters)
    {
        $this->parameters = $parameters;
        $this->viewPath = $viewPath;
        $this->templatePath = $templatePath;
    }

    /**
     * Rendering view file to a string
     *
     * @return string rendered view by string
     * @throws ViewException invalid parameter name
     */
    public function render(): string
    {
        foreach ($this->parameters as $name => $parameter) {
            if (!is_string($name) || strpos($name, ' ') !== false) {
                throw new ViewException('invalid variable name');
            }
            $$name = $parameter;
        }

        if(file_exists($this->viewPath)) {
            ob_start();
            include($this->viewPath);
            $content = ob_get_clean();
            ob_end_flush();
            if ($this->templatePath === null) {
                return $content;
            }
            return $this->renderTemplate($content);
        } else {
            throw new ViewException('view file: '.$this->viewPath.' not found');
        }
    }

    /**
     * @param string $content
     * @return string
     * @throws ViewException
     */
    public function renderTemplate(string $content): string
    {
        if(file_exists($this->templatePath)) {
            ob_start();
            include($this->templatePath);
            $content = ob_get_clean();
            ob_end_flush();
            return $content;
        } else {
            throw new ViewException('view file: '.$this->viewPath.' not found');
        }
    }
}