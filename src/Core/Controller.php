<?php

namespace Core;

use Doctrine\ORM\EntityManager;

/**
 * Class Controller
 * @package Core
 */
class Controller
{
    /**
     * @var EntityManager
     */
    private EntityManager $entityManager;

    /**
     * @var string views folder path
     */
    private string $viewsFolder;

    /**
     * Controller constructor.
     *
     * @param EntityManager $entityManager
     * @param string $viewsFolder views folder path
     */
    public function __construct(EntityManager $entityManager, string $viewsFolder)
    {
        $this->entityManager = $entityManager;
        $this->viewsFolder = $viewsFolder;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * View rendering
     *
     * @param string $viewPath html
     * @param array $parameters array like ['variableName' => $value, ...]
     * @param string|null $template
     * @return Response
     * @throws Exception\ViewException
     */
    protected function render(string $viewPath, array $parameters = [], ?string $template = null): Response
    {
        return new Response((
            new View($this->getAbsoluteViewPath($viewPath),
                $template === null ? null : $this->getAbsoluteViewPath($template),
                $parameters
            ))->render());
    }

    /**
     * Returns absolute view path
     *
     * @param string $viewPath
     * @return string
     */
    protected function getAbsoluteViewPath(string $viewPath): string
    {
        return $this->viewsFolder.$viewPath.'.php';
    }
}