<?php

namespace Factory;

use Repository\PictureSizeRepository;
use Service\GalleryGeneratorProcession;
use Service\ImageCacheManager;
use Service\ImageLoader;
use Service\ImageResizer;

/**
 * Class GalleryGeneratorProcessionFactory
 * @package Factory
 */
class GalleryGeneratorProcessionFactory
{
    /**
     * @param PictureSizeRepository $pictureSizeRepository
     * @param string $sourceFolder
     * @param string $cacheFolder
     * @return GalleryGeneratorProcession
     */
    public function make(PictureSizeRepository $pictureSizeRepository, string $sourceFolder, string $cacheFolder): GalleryGeneratorProcession
    {
        return new GalleryGeneratorProcession(
            $pictureSizeRepository,
            new ImageCacheManager(),
            new ImageResizer(),
            new ImageLoader(),
            $sourceFolder,
            $cacheFolder
        );
    }
}