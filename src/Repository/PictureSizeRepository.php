<?php

namespace Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Entity\PictureSize;

/**
 * Class PictureSizeRepository
 * @package Repository
 */
class PictureSizeRepository extends EntityRepository
{

    /**
     * @param string $code
     * @return PictureSize
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getByCode(string $code): PictureSize
    {
        return $this->createQueryBuilder('size')
            ->andWhere('size.code = :code')
            ->setParameter(':code', $code)
            ->getQuery()
            ->useQueryCache(true)
            ->enableResultCache()
            ->getSingleResult();
    }

    /**
     * Get all sizes, except $excepted
     *
     * @param array $excepted
     * @return array
     */
    public function getSizesExcept(array $excepted): array
    {
        $qb = $this->createQueryBuilder('size');
        return $qb->andWhere($qb->expr()->notIn('size.code', $excepted))
            ->addOrderBy('size.width', 'DESC')
        ->getQuery()
        ->getResult();
    }
}