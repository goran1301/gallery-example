<?php

namespace Exception;

class FormatNotSupportedException extends ImageException
{
}