<?php
use Service\Gallery;
/** @var Gallery $gallery */
?>
<div id="#galleryContent">
<?php foreach($gallery->getImageList() as $image): ?>
    <div class="galleryItem" style="display: inline-block; margin: 5px; border: black solid 8px;">
        <img src="/generator.php?image=<?=$image+1?>&size=<?=$gallery->previewSizeCode()?>"
             data-name="<?=$image+1?>"
             alt="<?=$image+1?>"
             class="preview"
        >
    </div>
<?php endforeach;?>
</div>
<div class="modal" id="galleryModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("img.preview").each(function( index ) {
            $(this).click(function () {
                $('#galleryModal .modal-body').html(
                    '<img src = /generator.php?image='+ $(this).attr('data-name') +'&size=med style="max-width: 100%">' +
                    '<img src = /generator.php?image='+ $(this).attr('data-name') +'&size=big style="max-width: 100%">'
                );
                $('#galleryModal').modal('show');
            });
        });
    })
</script>