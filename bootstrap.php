<?php

use Core\BeforeRequest;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

BeforeRequest::job();

$isDevMode = true;
$proxyDir = null;
$cache = new FilesystemCache(__DIR__.'/doctrine_cache');
$useSimpleAnnotationReader = false;

$config = Setup::createAnnotationMetadataConfiguration(
    [__DIR__."/src/Entity"],
    $isDevMode,
    $proxyDir,
    $cache,
    $useSimpleAnnotationReader
);

$conn = [
    'dbname' => 'gallery',
    'user' => 'root',
    'password' => 'root',
    'host' => 'localhost',
    'driver' => 'pdo_mysql',
];

$entityManager = EntityManager::create($conn, $config);